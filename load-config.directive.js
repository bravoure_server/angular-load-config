(function(){
    'use strict';

    function loadConfig ($rootScope) {
        return {
            restrict: 'EA',
            controller: function () {
                $rootScope.config = {
                    'company_name': data.company_name
                }
            }
        }
    }

    loadConfig.$inject = ['$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('loadConfig', loadConfig);

})();
